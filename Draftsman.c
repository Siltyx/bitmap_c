#include "Bitmap_Data_Types.h"
#include "Bitmap_Structs.h"
#include <stdbool.h>
#define COLOR_MAX 255

void drawRectangle(int xL, int xH, int yL, int yH, int width, int height, RGB24 pixels24[][width]){
    for(int i = xL-1; i <= xH+1; i++){
        if(i > 0 && i < width){                     //check corners
            if(yL-1 >= 0){                              //lower horizontal line
                pixels24[yL-1][i].blue  = COLOR_MAX;
                pixels24[yL-1][i].red   = 0;
                pixels24[yL-1][i].green = 0; 
            }

            if(yH+1 < height){                           //higher horizontal line
                pixels24[yH+1][i].blue  = COLOR_MAX;
                pixels24[yH+1][i].red   = 0;
                pixels24[yH+1][i].green = 0;
            }
        }
    }
    for(int i = yL; i <= yH; i++){                  //left side vertical line
        if(xL-1 >= 0){
            pixels24[i][xL-1].blue  = COLOR_MAX;
            pixels24[i][xL-1].red   = 0;
            pixels24[i][xL-1].green = 0; 
        }
        
        if(xH+1 < width){                           //right side vertical line
            pixels24[i][xH+1].blue  = COLOR_MAX;
            pixels24[i][xH+1].red   = 0;
            pixels24[i][xH+1].green = 0;
        }
    }
    
}

