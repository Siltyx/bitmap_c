#include "Bitmap_Structs.h"
#include "Bitmap_Data_Types.h"
#include <stdio.h>
#define UNUSED 0
#define BYTES_COLORS 1024
#define BIT24 24
void make8to24BitArray(BMIH_Ptr infoH, BYTE pixels[][infoH->biWidth], RGB colors[], RGB24 pixel24[][infoH->biWidth]){
    for(int i = 0; i<(infoH->biHeight); i++){
        for(int j = 0; j<(infoH->biWidth); j++){
            pixel24[i][j].blue  = colors[pixels[i][j]].blue;    //save the pixel into pixel24 array by its corresponding color value in the color table
            pixel24[i][j].red   = colors[pixels[i][j]].red;
            pixel24[i][j].green = colors[pixels[i][j]].green;
        }
    }
}
 
void save24BitMap(BMFH_Ptr fileH, BMIH_Ptr infoH, RGB24 pixel24[][infoH->biWidth], FILE  *bmp_copy){
    BMFH fh24 = {fileH->bfType, UNUSED, UNUSED,UNUSED, fileH->bfOffBits-BYTES_COLORS};                                                      //Save new Fileheader, unused values are not necessary
    BMIH ih24 = {infoH->biSize, infoH->biWidth, infoH->biHeight, infoH->biPlanes, BIT24, UNUSED, UNUSED, UNUSED, UNUSED, UNUSED, UNUSED};   //Save new Fileheader, unused values are not necessary
    fwrite(&fh24, 1, sizeof(BMFH), bmp_copy); //save into copy
    fwrite(&ih24, 1, sizeof(BMIH), bmp_copy); //save into copy
    
    //save array as bitmap
    for(int i = 0; i<(infoH->biHeight); i++){
       fwrite(pixel24[i], sizeof(RGB24), infoH->biWidth, bmp_copy);
    }
}