/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bitmap_structs.h
 * Author: Sebastian
 *
 * Created on 29. April 2016, 13:00
 */

#ifndef BITMAP_STRUCTS_H
#define BITMAP_STRUCTS_H
#include "Bitmap_Data_Types.h"

struct BITMAPFILEHEADER {
  WORD  bfType;
  DWORD bfSize;
  WORD  bfReserved1;
  WORD  bfReserved2;
  DWORD bfOffBits;
} __attribute__ ((packed));
typedef struct BITMAPFILEHEADER BMFH;
typedef BMFH *BMFH_Ptr;


struct BITMAPINFOHEADER {
  DWORD biSize;
  LONG  biWidth;
  LONG  biHeight;
  WORD  biPlanes;
  WORD  biBitCount;
  DWORD biCompression;
  DWORD biSizeImage;
  LONG  biXPelsPerMeter;
  LONG  biYPelsPerMeter;
  DWORD biClrUsed;
  DWORD biClrImportant;
} __attribute__ ((packed));
typedef struct BITMAPINFOHEADER BMIH;
typedef BMIH *BMIH_Ptr;

struct RGBCOLOR{
    BYTE blue;
    BYTE green;
    BYTE red;
    BYTE reserved;
}__attribute__ ((packed));
typedef struct RGBCOLOR RGB;
typedef RGB *RGB_Ptr;

struct RGBCOLOR24{
    BYTE blue;
    BYTE green;
    BYTE red;
}__attribute__ ((packed));
typedef struct RGBCOLOR24 RGB24;
typedef RGB24 *RGB24_Ptr;

struct RLE8PIXEL{
    BYTE nbr;
    BYTE color;
}__attribute__ ((packed));
typedef struct RLE8PIXEL RLE8;
typedef RLE8 *RLE8_Ptr;

struct COORDINATE{
    int Y;
    int X;
};
typedef struct COORDINATE YX;

#endif /* BITMAP_STRUCTS_H */

