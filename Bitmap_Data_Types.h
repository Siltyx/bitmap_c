/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bitmap_Data_Types.h
 * Author: Sebastian
 *
 * Created on 29. April 2016, 13:01
 */

#ifndef BITMAP_DATA_TYPES_H
#define BITMAP_DATA_TYPES_H

typedef char CHAR;
typedef short SHORT;
typedef int LONG;
typedef unsigned int DWORD;
typedef int BOOL;
typedef unsigned char BYTE;
typedef unsigned short WORD;

#endif /* BITMAP_DATA_TYPES_H */

