/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RectangleFinder.h
 * Author: Sebastian
 *
 * Created on 1. Mai 2016, 12:06
 */


#ifndef RECTANGLEFINDER_H
#define RECTANGLEFINDER_H

int checkPixelRow(int Y, int X, BYTE *pixels, int loHi);

int setParameter(int arHeight, int arWidth);

int checkPixelBounds(int Y, int X1, int X2,  BYTE *pixels);

YX findRectangle(int Y, int X, BYTE *pixels);

#endif /* RECTANGLEFINDER_H */

