/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Decode.h
 * Author: Sebastian
 *
 * Created on 29. April 2016, 15:43
 */

#ifndef DECODE_H
#define DECODE_H

void RLE8decode (BMIH_Ptr infoH, BYTE pixels[][infoH->biWidth], FILE *bitmap);

#endif /* DECODE_H */


