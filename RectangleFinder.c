#include "Bitmap_Data_Types.h"
#include "Bitmap_Structs.h"
#include <stdbool.h>
#define MIN_RECT_SIZE 7 //Min height of a rectangle to recognize it as one

int width;
int height;


int checkPixelRow(int Y, int X, BYTE pixels[][width], int loHi){
    BYTE startPixel = pixels[Y][X];                             //Start color of input pixel
    while(X<width){
        X++;                                                    //increment x value for next pixel 
        if(pixels[Y][X] == startPixel){                         //compare Pixel with next one
            if( pixels[Y-loHi][X] == startPixel ) return X-1;   //check Pixel below or hiher if they are also blue
        } else return X-1;                                      //Not blue anymore
    }  
    return X-1;   //nothing found
 }

int setParameter(int arHeight, int arWidth){    //set parameters for rectangle finder
    height = arHeight;
    width  = arWidth;
}

int checkPixelBounds(int Y, int XL, int XR,  BYTE pixels[][width]){
    BYTE startPixel = pixels[Y][XL];                                            //start color
    while(Y<height){
        Y++;                                                                    //increment 
        if(pixels[Y][XL-1] == startPixel || pixels[Y][XR+1] == startPixel){     //check outerbounds
            return Y-1;
        }
        if(pixels[Y][XL] != startPixel || pixels[Y][XR] != startPixel){         //check next two higher pixels
            return Y-1;
        }
    }
    return 0;
}

YX resu = {0,0};                                                //Initialise a returnable result

YX findRectangle(int Y, int X, BYTE pixels[][width]){           //given pixel is the lower left corner of a possible rectangle
    int lowerRow = checkPixelRow(Y, X, pixels, 1);              //         
    if(lowerRow == X){  
        resu.Y = -1;                                            //y-1 = nothing found
        resu.X = lowerRow;                                      //to skid already checked pixel
        return resu;
    }
    int maxBounds =  checkPixelBounds(Y, X, lowerRow, pixels);
    if(maxBounds < (Y+MIN_RECT_SIZE)){
        resu.Y = -1;                                                //y-1 = nothing found
        resu.X = lowerRow;                                          //to skid already checked pixel
        return resu;
    }
    
    int higherRow = checkPixelRow(maxBounds, X, pixels, -1);
    if(higherRow == lowerRow){       //if the end value of the lower and higher line are equal, its a rectangle                           
        resu.Y = maxBounds;            //XY values for end of rectangle 
        resu.X = higherRow;
        return resu;
    } else {
        resu.Y = -1;                //y-1 = nothing found
        resu.X = lowerRow;          //to skid already checked pixel
        return resu;
    }
}
