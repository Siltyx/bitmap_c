/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Sebastian
 *
 * Created on 28. April 2016, 15:53
 */

#include <stdio.h>
#include <stdlib.h>
#include "Output.h"
#include "Bitmap_Structs.h"
#include "Bitmap_Data_Types.h"
#include "Decode.h"
#include "RectangleFinder.h"
#include "Draftsman.h"
#define EXIT_FAILURE 1
#define BITMAP_CHECK    0x4D42

int main(int argc, char** argv) {
    
    FILE *bmp_pointer; //File opened   
    FILE *bmp_copy;    //Mod File 
     
    BMFH_Ptr fh = malloc(sizeof(BMFH)); //FileHeader
    BMIH_Ptr ih = malloc(sizeof(BMIH)); //InfoHeader    
    
    int nbrR = 0;
        
    if ((bmp_pointer = fopen("Z:\\test_3.bmp","r")) == NULL){   //open stream for reading
        printf("Error opening file %s.\n","test.bmp");
        exit(EXIT_FAILURE);
    } else {
        printf("File open \n") ;
    }
     
    if ((bmp_copy = fopen("Z:\\test_4.bmp","w")) == NULL){      //open stream for writing
        printf("Error opening file %s.\n","test.bmp");
        exit(EXIT_FAILURE);
    } else {
        printf("Copy open \n") ;
    }
     
    fread(fh, 1, sizeof(BMFH), bmp_pointer); //read the fileHeader
    if(fh==NULL){
        printf("Couldnt read Fileheader or couldnt reserve Memory");
        exit(EXIT_FAILURE);
    }
    
    fread(ih, 1, sizeof(BMIH), bmp_pointer); //read the infoHeader
    if(ih==NULL){
        printf("Couldnt read Infoheader or couldnt reserve Memory");
        exit(EXIT_FAILURE);
    }
    
    //Check if it is a Bitmap
   if(fh->bfType != BITMAP_CHECK){
       printf("Not a Bitmap");
       exit(EXIT_FAILURE);
   }
    
    //Check if Bitmap is of uneven size
    if(ih->biWidth %2 != 0 || ih->biHeight %2 != 0 ){
        printf("No uneven Bitmap size supported");
        exit(EXIT_FAILURE);
    }
    
    //Unsuported Format
    if(ih->biCompression != 1 || ih->biBitCount != 8){
        printf("Only 8 bit RLE8 compressed supported");
        exit(EXIT_FAILURE);
    }
    
    
    //Set lowest and highest values for the drawing of the Rectangle to values that every found rectangle must change them 
    int yH = -1;
    int yL = ih->biHeight; 
    int xH = -1;
    int xL = ih->biWidth;
    
     
    BYTE pixels[ih->biHeight][ih->biWidth];    //Array mit 8bit pixeln
    RGB24 pixel24[ih->biHeight][ih->biWidth];  //Arary mit 24 bit pxeln
    RGB colors[256];                           //Array mit farben
    fread(colors, sizeof(BYTE), 1024, bmp_pointer);    // Einlesen der Farben  
    fseek(bmp_pointer, fh->bfOffBits, SEEK_SET);
    
    //Error check
    if( pixels  ==  NULL ||     
        pixel24 ==  NULL ||
        colors  ==  NULL ||
        bmp_pointer == NULL){
            printf("Something went wrong!");
            exit(EXIT_FAILURE);
                
    }
        
    
        //Begin analysing bitmap
        int wIndex = 0;         //index for pixel to check X
        int hIndex = 0;         //index for pixel to check Y
        
        RLE8decode(ih, pixels, bmp_pointer);        //decode Bitmap
        setParameter(ih->biHeight, ih->biWidth);    //set parameters for Analysis
        
        
        while(hIndex < ih->biHeight ){ //Every Row
            while(wIndex < ih->biWidth){    //Every pixel in Row
//                while(pixels[hIndex][wIndex]==215){
//                    wIndex++;
//                    if(wIndex+1==ih->biWidth){
//                        hIndex++;
//                        wIndex = 0;
//                    }
//                }
                YX resu = findRectangle(hIndex, wIndex, pixels); 
                if(resu.Y != -1){
                    
                    //Set new max Values for Rectangle
                    if(xL > wIndex){
                        xL = wIndex;
                    }
                    if(yL > hIndex){
                        yL = hIndex;
                    }
                    if(xH < resu.X){
                        xH = resu.X;
                    }
                    if(yH < resu.Y){
                        yH = resu.Y;
                    }              
                    
                }
                wIndex = resu.X + 1;
            }
            wIndex = 0;
            hIndex++;
        }

    
    
    make8to24BitArray(ih, pixels, colors, pixel24);                     //convert Array from 8bit to 24Bit
    drawRectangle(xL, xH, yL, yH, ih->biWidth, ih->biHeight, pixel24);  //draw the rectangle
    save24BitMap(fh, ih, pixel24, bmp_copy);                            //save the Bitmap
    
    
    //Free and close everything
    fclose(bmp_pointer);
    fclose(bmp_copy);
    free(fh);
    free(ih);
    
    
    
     
    return 0;
}

