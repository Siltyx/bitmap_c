#include "Bitmap_Data_Types.h"
#include "Bitmap_Structs.h"
#include <stdio.h>
#include <stdlib.h>

void RLE8decode (BMIH_Ptr infoH, BYTE pixels[][infoH->biWidth], FILE *bitmap){
    RLE8 input = {0,0};
    int hIndex = 0;
    int wIndex = 0;
    BYTE abMode = 0;
    
    fread(&input, 1, sizeof(RLE8), bitmap);
    while( !(input.nbr == 0 && input.color == 1)){ //end of file
        if(input.nbr == 0){                        //if nbr == 0 -> special encoding
            
            if(input.color == 0){               //next bitmap line
                hIndex++;
                wIndex = 0;
               // printf("%d | ", hIndex );
            }
                
            if(input.color == 2){               //delta
                printf("\n Delta unsupported");
                exit(EXIT_FAILURE);
            }
            
            if(input.color >= 3){               //absolute mode
                for(int i = 0; i < input.color; i++){
                    fread(&abMode, 1, 1, bitmap);       //Read one Byte into Byte
                    pixels[hIndex][wIndex] = abMode;    //Put Byte ito bytemap         
                    if(wIndex+1 == infoH->biWidth){     //Newline in dierect mode
                        hIndex++;
                        wIndex = 0;
                    }
                    wIndex++;   
                }
                if(!(input.color % 2 == 0)){ //ignore next byte if direct mode was odd
                    fseek(bitmap, 1, SEEK_CUR);
                }
            }
        } else {
            for(int i = 0; i < input.nbr && wIndex < infoH ->biWidth; i++){ //normal RLE8 mode
                pixels[hIndex][wIndex]  = input.color;
                wIndex++;
            }
        }
        fread(&input, 1, sizeof(RLE8), bitmap); //read next 2 bytes
    }
    
}
