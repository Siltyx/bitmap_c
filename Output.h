/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Output.h
 * Author: Sebastian
 *
 * Created on 29. April 2016, 14:04
 */

#ifndef OUTPUT_H
#define OUTPUT_H
#include "Bitmap_Data_Types.h"
#include "Bitmap_Structs.h"

void make8to24BitArray(BMIH_Ptr infoH, BYTE pixels[][infoH->biWidth], RGB colors[], RGB24 pixel24[][infoH->biWidth]);

void save24BitMap(BMFH_Ptr fileH, BMIH_Ptr infoH, RGB24 pixel24[][infoH->biWidth], FILE  *bmp_copy);

#endif /* OUTPUT_H */

